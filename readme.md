# [SASS](http://sass-lang.com/) / [COMPASS](http://compass-style.org/), [SUSY](http://susy.oddbird.net/) & [GRUNT.JS](http://gruntjs.com/) #

This project is a simple set up for a web project that uses [SASS](http://sass-lang.com/), [COMPASS](http://compass-style.org/), [SUSY](http://susy.oddbird.net/) & [GRUNT.JS](http://gruntjs.com/). It is used for a personal sanity/check list when I am setting up something new. It is based on a tutorial given by [Ray Villalobos](https://twitter.com/planetoftheweb). 

The idea is to create a standard workflow when doing front-end development without the use of apps like CODEKIT etc. 

This project compiles [SASS](http://sass-lang.com/) and JS files using [GRUNT.JS](http://gruntjs.com/) as the task manager everytime a save is detected as well as live reloading into a browser window. It also uses [SUSY](http://susy.oddbird.net/), a responsive grid framework.

# Initial Setup #
Basic folder structure would be;

```
Main Directory
* resources
	* components
		* sass
		* js
	* css
	* js
* index.html
```
# Installation #
## GIT ##
Download the [latest package] (http://git-scm.com/book/en/Getting-Started-Installing-Git)

Opening Terminal and CD to the projects directory. Then to create an empty repo

```
git init
```

Add all files to staging environment 

```
git add .
```
```
git commit -m "First commit"
```

We can then create a standard git ignore file. Firstly we create an .gitignore. [Here is a standard one] (https://gitlab.com/mustafa/sass-and-susy-setup/blob/master/.gitignore) which has added *.sass-cache* to the bottom. 

## NODE JS ##
Firstly check if we have NODE.JS installed. In terminal type;

```
node -v
```

If node is NOT installed we can [download it here](http://nodejs.org/)

Now we need to set up the package in order to install our dependencies. Create a new file package.json and add it to the main directory. The dependencies are Grunt, Grunt Watch, [COMPASS](http://compass-style.org/) & Uglify.

In the file we type;
```
{
	"name" : "project_name",
	"version" : "0.0.1",
	"dependencies" : {
		"grunt" : "~0.4.1",
		"grunt-contrib-watch" : "~0.5.3",
		"grunt-contrib-compass" : "~0.5.0",
		"grunt-contrib-uglify" : "~0.2.2",
		"matchdep" : "~0.1.2"
	}
}
```
The ~VERSION_NUMBER is so we get an approximate version, so if another version is found we can still use it. 

With the package recreated we install by;

```
npm install
```

This will create a new folder called `node_modules`.

## [GRUNT.JS](http://gruntjs.com/) ##
With Node.js installed we can run the Node Package Manager again to install Grunt. We may need to do a sudo command to get this working. 

```
sudo npm install -g grunt-cli
```

Now we need to create the Grunts Tasks file called `gruntfile.js` and add it to the main directory. This file loads the tasks for minifying our JS files, watching specified files for changes (HTML, JS & [SASS](http://sass-lang.com/)) and [COMPASS](http://compass-style.org/) tasks which are fired when required. 

```
module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.initConfig({
		uglify : {
			my_target : {
				files: {
					'resources/js/script.js' : ['resources/components/js/*.js']
				} // files
			} // my_target
		}, // uglify
		compass: {
			dev: {
				options: {
					config: 'config.rb'
				} // options
			} // dev
		}, // compass
		watch: {
			options: { livereload: true },
			scripts: {
				files: ['resources/components/js/*.js'], 
				tasks: ['uglify']
			}, // scripts
			sass: {
				files: ['resources/components/sass/*.scss'],
				tasks: ['compass:dev']
			}, // sass
			html : {
				files: ['*.html']
			}
		}// watch
	}) // initConfig
	grunt.registerTask('default', 'watch'); // auto watch
} // exports 
```

The WATCH object has a `livereload: true` set which forces a reload when changes have been made to specific files. We now need to add a line of JS to the HTML page; 

```
	<script src="http://localhost:35729/livereload.js" type="text/javascript"></script>
```

Now we need to add [SASS](http://sass-lang.com/) and [COMPASS](http://compass-style.org/) which require [RUBY](https://www.ruby-lang.org/en/downloads/) 

With Ruby installed we first update the system by 
```
gem update --system
```

Then we install [COMPASS](http://compass-style.org/)

```
gem install compass
```

Now we create our Ruby config file called `config.rb` and add it to the main directory. This is telling [SASS](http://sass-lang.com/) and [COMPASS](http://compass-style.org/) where our .js and .scss file directories are. In the file we add;

```
require 'susy'
css_dir = 'resources/css'
sass_dir = 'resources/components/sass'
javascript_dir = 'resources/js'
output_style = :compressed
relative_assets = true
```
We can use expanded CSS / JS output for debuging by changing `output_style = :expanded`. 

## [SUSY](http://susy.oddbird.net/) ##
This is for Responsive design grids. To install type in terminal;
```
gem install susy
```

# To run #
CD to the directory in terminal and type; 
```
grunt
```

This will start the watch task we specifed in the `gruntfile.js` which will wait for changes to the files listed and recompile them or reload the browser window. You can see the logs in the terminal.

# Sublime Text Plug-in's #
For quick coding : [Emmet](https://github.com/sergeche/emmet-sublime)

# Create SASS Partials #

```
* resources
	* components
		* sass
			_base.scss
			_layout.scss
			_mixins.scss
			_modules.scss
			_variables.scss
			styles.scss
```

In `styles.scss` we add

```
@import "compass";
@import "compass/reset";
@import "susy";

@import "variables";
@import "mixins";
@import "base";
@import "layout";
@import "modules";
```

If we want to use the old susy syntax then we can write `@import "susyone";` instead of `@import "susy";`

# FIN #

The whole directory structure should look like this; 

```
* config.rb
* index.html
* gruntfile.js
* node_modules
* package.json
* resources
	* components
		* sass
			_base.scss
			_layout.scss
			_mixins.scss
			_modules.scss
			_variables.scss
			styles.scss
		* js
			script.js
	* css
		styles.css
	* js
		script.js
```
